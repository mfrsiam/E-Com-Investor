from django.shortcuts import render

# Create your views here.

def HomeView(request):
    return render(request,'index.html')

def LoginView(request):
    return render(request,'login.html')

def FeedView(request):
    return render(request,'feed.html')