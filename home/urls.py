from django.urls import path

from .views import HomeView,LoginView,FeedView



urlpatterns = [
    path('',HomeView,name='home'),
    path('login/',LoginView,name='login'),
    path('feed/',FeedView,name='feed'),
]